import java.util.*;



public class Solution {

    public int[] twoSum(int[] numbers, int target) {
        if (numbers == null) {
            throw new IllegalArgumentException();
        }

        HashMap<Integer, Integer>map = new HashMap<Integer, Integer>();
        for (int i = 0; i < numbers.length; i++) {
            map.put(numbers[i], i);
        }

        int[] result = new int[2];
        for (int i = 0; i < numbers.length; i++) {
            if (map.containsKey(target - numbers[i])) {
                int j = map.get(target - numbers[i]) + 1;
                i++;
                result[0] = (i < j) ? i : j;
                result[1] = (i < j) ? j : i;
                return result;
            }
        }

        return result;
    }
}